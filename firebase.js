import firebase from 'firebase';
import firestore from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyDzms2wEzW2U7PVDIgsp0-C6tKd5ZxhVJM",
  authDomain: "project09-de9e7.firebaseapp.com",
  databaseURL: "https://project09-de9e7.firebaseio.com",
  projectId: "project09-de9e7",
  storageBucket: "project09-de9e7.appspot.com",
  messagingSenderId: "146839210590",
  appId: "1:146839210590:web:4ef4e87e61d5b65ec142cf",
  measurementId: "G-6GWXMYLZ2S"
};

  firebase.initializeApp(firebaseConfig);
  firebase.firestore();

  export default firebase;